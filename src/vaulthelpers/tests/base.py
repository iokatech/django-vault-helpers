import os

import hvac
import vaulthelpers
from django.contrib.auth.models import User
from django.db import connection
from django.test import TransactionTestCase


class VaultHelperTest(TransactionTestCase):
    def get_session_and_current_user(self, retries=1):
        with connection.cursor() as cursor:
            try:
                cursor.execute("SELECT SESSION_USER, CURRENT_USER;")
            except: # noqa
                if retries == 0:
                    raise
                return self.get_session_and_current_user(0)
            session_user_A, current_user_A = cursor.fetchone()
            return session_user_A, current_user_A

    def setUp(self):
        # Close the database connection
        connection.close()
        connection.connection = None

        # Revoke all the non-root tokens in Vault
        root_client = self.get_root_client()
        accessors = root_client.adapter.get('/v1/auth/token/accessors?list=true')['data']['keys']
        for accessor in accessors:
            params = { 'accessor': accessor }
            token_meta = root_client.adapter.post('/v1/auth/token/lookup-accessor', json=params)
            if token_meta['data']['display_name'] == 'approle':
                root_client.adapter.post('/v1/auth/token/revoke-accessor', json=params)

        # Reset the vault cache
        vaulthelpers.common.reset_vault()

        # Create a Django user
        User.objects.create_user(username='root', email='root@example.com')


    def get_root_client(self):
        return hvac.Client(url=vaulthelpers.common.VAULT_URL, token=os.environ['VAULT_ROOT_TOKEN'])
